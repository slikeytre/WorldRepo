# FTO Map Repository
This is a Fantasy Tales Online world reposity. To edit this repo please download the map editor from:

Windows: https://s3-us-west-2.amazonaws.com/ftoreleases/releases/live/trunk/client/FTOEditor.exe

Mac OSX: https://s3-us-west-2.amazonaws.com/ftoreleases/releases/live/trunk/client/FTOEditor.dmg

# Documentation

The documentation covers how to use the editor, setup your region, and much more:

[Map Editor Documentation](http://fantasytalesonline.gamepedia.com/Map_Editor)